import { PacienteService } from './../../../services/paciente.service';
import { Paciente } from './../../../model/paciente';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-paciente-edicion',
  templateUrl: './paciente-edicion.component.html',
  styleUrls: ['./paciente-edicion.component.css']
})
export class PacienteEdicionComponent implements OnInit {
  form:FormGroup;
  id:number;
  paciente:Paciente;
  edicion:boolean=false;
  constructor(private route:ActivatedRoute,private router:Router,private pacienteService:PacienteService) { 
    this.form=new FormGroup({
      'id': new FormControl(0),
      'apellido':new FormControl(''),
      'enfermedad':new FormControl(''),    
      'nombre':new FormControl(''),
      'rut':new FormControl(''),          
    }); 
  }
  initForm(){
    if(this.edicion){
      this.pacienteService.listarPorId(this.id).subscribe(data=>{
        this.form=new FormGroup({
          'id': new FormControl(data.id),
          'apellido':new FormControl(data.apellido),
          'enfermedad':new FormControl(data.enfermedad),   
          'nombre':new FormControl(data.nombre),
          'rut':new FormControl(data.rut),
        });
      })
        
    }
  }
  ngOnInit() {
    this.paciente=new Paciente();
    this.route.params.subscribe((params: Params)=>{
      this.id=params['id'];
      this.edicion=params['id']!=null;
      this.initForm();      
    });
  }
  operar(){
    this.paciente.id=this.form.value['id'];
    this.paciente.apellido=this.form.value['apellido'];
    this.paciente.enfermedad=this.form.value['enfermedad'];
    this.paciente.nombre=this.form.value['nombre'];
    this.paciente.rut=this.form.value['rut'];
    if(this.edicion){

      this.pacienteService.modificar(this.paciente).subscribe(data=>
      {
        this.pacienteService.listar().subscribe(pacientes => {this.pacienteService.pacienteCambio.next(pacientes);       
        this.pacienteService.mensajeCambio.next("Se realizo el cambio de manera exitosa!");
      });
    });
    
    }
    else{
      this.pacienteService.registar(this.paciente).subscribe(data=>{
        this.pacienteService.listar().subscribe(pacientes => {this.pacienteService.pacienteCambio.next(pacientes);       
        this.pacienteService.mensajeCambio.next("Guardado exitoso!");
        });
      });
      
    }
    this.router.navigate(['pacientes']);
  }

}
