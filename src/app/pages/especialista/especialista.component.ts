import { EspecialistaService } from "../../services/especialistaservice";
import { Especialista } from './../../model/especialista';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-especialista',
  templateUrl: './especialista.component.html',
  styleUrls: ['./especialista.component.css']
})
export class EspecialistaComponent implements OnInit {

  dataSource:MatTableDataSource<Especialista>;
  displayedColumns=['id','apellido','nombre','rut','acciones'];
  @ViewChild(MatPaginator) paginator:MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(private especialistaService:EspecialistaService,private snackBar:MatSnackBar) { }

  ngOnInit() {
    this.especialistaService.especialistaCambio.subscribe(data => {
      this.dataSource=new MatTableDataSource(data);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;
    });
    this.especialistaService.mensajeCambio.subscribe(data => {
      this.snackBar.open(data,'Aviso',{duration:2000});
    });
    
    this.especialistaService.listar().subscribe(data=>{
      this.dataSource=new MatTableDataSource(data);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;
    });
  }
  eliminar(id:number){
    this.especialistaService.eliminar(id).subscribe(data => {
      this.especialistaService.listar().subscribe(data =>{
        this.especialistaService.especialistaCambio.next(data);
        this.especialistaService.mensajeCambio.next('Se elimino');
      });
    });
  }
  applyFilter(filterValue:string){
    filterValue=filterValue.trim();
    filterValue=filterValue.toLowerCase();
    this.dataSource.filter=filterValue;
  }
}
