import { EspecialistaService } from "../../../services/especialistaservice";
import { Especialista } from './../../../model/especialista';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-especialista-edicion',
  templateUrl: './especialista-edicion.component.html',
  styleUrls: ['./especialista-edicion.component.css']
})
export class EspecialistaEdicionComponent implements OnInit {
  form:FormGroup;
  id:number;
  especialista:Especialista;
  edicion:boolean=false;
  constructor(private route:ActivatedRoute,private router:Router,private especialistaService:EspecialistaService) { 
    this.form=new FormGroup({
      'id': new FormControl(0),
      'apellido':new FormControl(''),
      'nombre':new FormControl(''),
      'rut':new FormControl(''),           
    }); 
  }
  initForm(){
    if(this.edicion){
      this.especialistaService.listarPorId(this.id).subscribe(data=>{
        this.form=new FormGroup({
          'id': new FormControl(data.id),
          'apellido':new FormControl(data.apellido),
          'nombre':new FormControl(data.nombre),
          'rut':new FormControl(data.rut),
        });
      })
        
    }
  }
  ngOnInit() {
    this.especialista=new Especialista();
    this.route.params.subscribe((params: Params)=>{
      this.id=params['id'];
      this.edicion=params['id']!=null;
      this.initForm();      
    });
  }
  operar(){
    this.especialista.id=this.form.value['id'];
    this.especialista.apellido=this.form.value['apellido'];
    this.especialista.nombre=this.form.value['nombre'];
    this.especialista.rut=this.form.value['rut'];
    if(this.edicion){

      this.especialistaService.modificar(this.especialista).subscribe(data=>
      {
        this.especialistaService.listar().subscribe(especialistas => {this.especialistaService.especialistaCambio.next(especialistas);       
        this.especialistaService.mensajeCambio.next("Se realizo el cambio de manera exitosa!");
      });
    });
    
    }
    else{
      this.especialistaService.registar(this.especialista).subscribe(data=>{
        this.especialistaService.listar().subscribe(especialistas => {this.especialistaService.especialistaCambio.next(especialistas);       
        this.especialistaService.mensajeCambio.next("Guardado exitoso!");
        });
      });
      
    }
    this.router.navigate(['especialista']);
  }

}
