import { HttpClient } from '@angular/common/http';
import { HOST } from '../shared/var.constant';
import { Subject } from 'rxjs';
import { Especialista } from '../model/especialista';
import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class EspecialistaService {
  especialistaCambio = new Subject<Especialista[]>();
  mensajeCambio = new Subject<string>();
  url: string = `${HOST}/especialistas`;
  constructor(private http: HttpClient) { }
  listar() {
    return this.http.get<Especialista[]>(this.url);
  }
  modificar(especialista: Especialista) {
    console.log(this.url);
    console.log(especialista);
    return this.http.put(this.url, especialista);
  }
  registar(especialista: Especialista) {
    return this.http.post(this.url, especialista);
  }
  listarPorId(id: number) {
    return this.http.get<Especialista>(`${this.url}/${id}`);
  }
  eliminar(id: number) {
    return this.http.delete(`${this.url}/${id}`);
  }
}
